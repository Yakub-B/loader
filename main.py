"""
Usage:
    python main.py 100(1) 10(2) prod(3)
    where 1 - number of request per worker, 2 - number of workers, 3 - env to load
"""

import asyncio
import sys
import os
import time
from concurrent.futures import ProcessPoolExecutor

import aiohttp
from dotenv import load_dotenv


def get_url() -> str:
    base_url = os.environ.get('BASE_URL')
    return f'{base_url}steps'


def get_args() -> tuple[int, int, str]:
    """
    Returns:
        1) num of request for each worker
        2) num of workers
        3) env to load (local, prod, ...)
    """
    return int(sys.argv[1]) or 5, int(sys.argv[2]), sys.argv[3]


def get_step_data() -> dict:
    return {
        'patient_id': os.environ.get('PAT_ID'),
        'device_id': os.environ.get('DEVICE_ID'),
        'day_id': os.environ.get('DAY_ID'),
        'weight_left': 30,
        'weight_right': 20,
        'timestamp': time.time(),
        'program_id': os.environ.get('PROGRAM_ID'),
        'patient_weight': 80,
    }


def get_headers() -> dict:
    return {
        'Content-Type': 'application/json',
        'X-Api-Key': os.environ.get('AUTH_TOKEN'),
        'User-Agent': os.environ.get('USER_AGENT'),
    }


async def create_steps(count: int, coro_id: int) -> int:
    success_resp_count = 0
    async with aiohttp.ClientSession() as session:
        data = get_step_data()
        headers = get_headers()
        url = get_url()

        for i in range(1, count + 1):
            print(f'Running coro {coro_id}')
            async with session.post(url, json=data, headers=headers) as resp:
                success_resp_count += 1 if resp.status == 200 else 0

    return success_resp_count


def run_in_new_loop(count: int, coro_id: int) -> int:
    loop = asyncio.new_event_loop()
    result = loop.run_until_complete(create_steps(count, coro_id))
    loop.close()
    return result


def main():
    count, workers, env = get_args()
    load_dotenv(f'.env.{env}')
    with ProcessPoolExecutor(workers) as executor:
        futures = []
        start_at = time.time()
        for worker_id in range(workers):
            futures.append(executor.submit(run_in_new_loop, count, worker_id))

        while not all(list(map(lambda f: f.done(), futures))):
            pass

        stop_at = time.time()
        total_requests = count * workers
        successful_requests = sum(map(lambda f: f.result(), futures))
        rps = total_requests / (stop_at - start_at)
        success_percent = (successful_requests / total_requests) * 100

        print(f'RPS: {rps}')
        print(f'200%: {success_percent}')


if __name__ == '__main__':
    main()
